package com.vitu.riot.champion.domain;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;

@Getter
@Setter
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Skin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String value;
    private String theme;

    @ManyToOne
    @JoinColumn(name = "champion_id")
    private Champion champion;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Skin skin = (Skin) o;
        return id != null && Objects.equals(id, skin.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
