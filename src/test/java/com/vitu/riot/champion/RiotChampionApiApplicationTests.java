package com.vitu.riot.champion;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@Slf4j
@SpringBootTest
@ActiveProfiles(profiles = "test")
class RiotChampionApiApplicationTests extends AbstractDatabaseTest {

    @BeforeAll
    static void initLogger() {
        log.info("------------ General context test initiated ---------");
    }

    @Test
    void contextLoads() {
    }

}
