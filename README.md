# riot-champion-api

## Status

| Branch           | Main                                                                                                                                                                                                                           | Dev                                                                                                                                                            |
|------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Pipeline status  | [![pipeline status](https://gitlab.com/Norbertoooo/riot-champion-api/badges/main/pipeline.svg)]( https://gitlab.com/Norbertoooo/riot-champion-api/-/commits/main )                                                             | [![pipeline status](https://gitlab.com/Norbertoooo/riot-champion-api/badges/dev/pipeline.svg)]( https://gitlab.com/Norbertoooo/riot-champion-api/-/commits/dev ) |
| Coverage         | [![coverage report](https://gitlab.com/Norbertoooo/riot-champion-api/badges/main/coverage.svg)]( https://gitlab.com/Norbertoooo/riot-champion-api/-/commits/main )                                                             | [![coverage report](https://gitlab.com/Norbertoooo/riot-champion-api/badges/dev/coverage.svg)]( https://gitlab.com/Norbertoooo/riot-champion-api/-/commits/dev ) |
| Cucumber reports | [![CucumberReports: riot-champion-api](https://messages.cucumber.io/api/report-collections/66a519d7-47a1-4c2a-992f-9805b9cbb2de/badge)]( https://reports.cucumber.io/report-collections/66a519d7-47a1-4c2a-992f-9805b9cbb2de ) | [![CucumberReports: riot-champion-api-dev](https://messages.cucumber.io/api/report-collections/d02e3604-5146-4251-b049-67d96f9b462b/badge)](https://reports.cucumber.io/report-collections/d02e3604-5146-4251-b049-67d96f9b462b)                                                                                                                                                               |

## Objetivos

- [x] Cucumber [https://cucumber.io/]
- [x] Testcontainers [https://www.testcontainers.org/]
- [x] Cucumber Reports [https://reports.cucumber.io/]
- [x] Gitlab CI
- [x] Jacoco Coverage
- [x] Métricas com Prometheus [https://prometheus.io/]
- [x] Gráficos com Grafana [https://grafana.com/]
- [x] Checkstyle
- [x] Documentação [https://springdoc.org/]
- [ ] Consulta de apis externas com feign client
- [ ] Enums
- [x] Executar os testes de unidade e integração(cucumber) em apenas um comando (mvn test)
- [ ] Executar os testes de unidade e integração(cucumber) em comandos separados
- [x] CD (continuous delivery) com heroku [https://www.heroku.com/home]
- [ ] Adicionar instruções ao readme
- [ ] Logs centralizados
- [ ] Design pattern - strategy [https://refactoring.guru/pt-br/design-patterns/strategy/java/example]




